node default {
  # Save the trusted pp_role to a shorter variable so it's easier to work with.
  $role = $trusted['extensions']['pp_role']
  include role::common
  case $role {
    puppetdb: {
      include "role::$role"
    }
    client1: {
      include "role::$role"
    }
    undef, '': {
      fail("${trusted['certname']} does not have a pp_role trusted fact!")
    }
  }
}
